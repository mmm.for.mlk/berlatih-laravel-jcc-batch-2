<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthCOntroller extends Controller
{
    public function register()
    {
        return view('halaman.register');
    }

    public function welcome(Request $request)
    {
        // dd($request->all());
        $nama = $request['nama'];
        $belakang = $request['belakang'];
        return view('halaman.welcome', compact('nama','belakang'));
    }
}
