<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('Cast.create');
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required',
            ],
            [
                'nama.required' => 'Silahkan isi nama terlebih dahulu',
                'umur.required'  => 'Silahkan isi umur terlebih dahulu',
                'bio.required'  => 'Silahkan isi biodata terlebih dahulu',
            ]
        );

        DB::table('cast')->insert(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request ['bio']
            ]
        );

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('Cast.index', compact('cast'));
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('Cast.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('Cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required',
            ],
            [
                'nama.required' => 'Silahkan isi nama terlebih dahulu',
                'umur.required'  => 'Silahkan isi umur terlebih dahulu',
                'bio.required'  => 'Silahkan isi biodata terlebih dahulu',
            ]
        );

        DB::table('cast')->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio'],
                ]
            );

            return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();
        
        return redirect('/cast');
    }
}
