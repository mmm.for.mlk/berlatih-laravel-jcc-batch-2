@extends('layout.master')

@section('judul')
    Halaman Membuat Cast
@endsection

@section('content')
    <form action="/cast" method="post">
        @csrf
        <div class="form-group">
            <label for="namaCast">Nama Pemeran</label>
            <input type="text" class="form-control" id="namaCast" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="umurCast">Umur Pemeran</label>
            <input type="number" class="form-control" id="umurCast" name="umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="bioCast">Biodata Pemeran</label>
            <textarea name="bio" id="bioCast" class="form-control" cols="30" rows="10"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection