@extends('layout.master')

@section('judul')
    Edit Tentang Pemeran
@endsection

@section('content')
    <a href="/cast" class="btn btn-info btn-sm">
        <i class="right fas fa-angle-left"></i>
        Kembali
    </a>
    <br><br>

    <form action="/cast/{{$cast->id}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="namaCast">Nama Pemeran</label>
            <input type="text" value="{{$cast->nama}}" class="form-control" id="namaCast" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="umurCast">Umur Pemeran</label>
            <input type="number" value="{{$cast->umur}}" class="form-control" id="umurCast" name="umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="bioCast">Biodata Pemeran</label>
            <textarea name="bio" id="bioCast" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection