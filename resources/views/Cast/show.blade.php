@extends('layout.master')

@section('judul')
    Detail Tentang Pemeran
@endsection

@section('content')

<a href="/cast" class="btn btn-info btn-sm">
    <i class="right fas fa-angle-left"></i>
    Kembali
</a>
<br><br>

<p>Nama Pemeran :</p>
<h4>{{$cast->nama}}</h4>
<br>

<p>Umur Pemeran :</p>
<h4>{{$cast->umur}} tahun</h4>
<br>

<p>Biodata Pemeran :</p>
<h5>{{$cast->bio}}</h5>
<br>
<a href="/cast/{{$cast->id}}/edit" class="btn btn-warning btn-sm">Edit</a>

@endsection