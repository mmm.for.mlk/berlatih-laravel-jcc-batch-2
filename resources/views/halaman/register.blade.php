    @extends('layout.master')

    @section('judul')
       Buat Account Baru    
    @endsection
    
    @section('content')
        <h4>Sign Up Form</h4> <br>
        
        <form action="/send" method="post">
            @csrf
            <label for="name">First name :</label> <br>
            <input type="text" id="name" name="nama" required> <br> <br>

            <label for="last">Last name :</label> <br>
            <input type="text" id="last" name="belakang" required> <br> <br>

            <label for="Gender">Gender</label> <br>
            <input type="radio" name="Gender" id="Gender" value="1" required> Male <br>
            <input type="radio" name="Gender" id="Gender" value="2" required> Female <br> <br>

            <label for="national">Nationality</label> <br>
            <select name="nationality" id="national" required>
                <option value="1">Indonesia</option>
                <option value="2">Malaysia</option>
                <option value="3">Thailand</option>
            </select> <br> <br>

            <label for="language">Language Spoken</label> <br>
            <input type="checkbox" name="bahasa" id="language">Bahasa Indonesia <br>
            <input type="checkbox" name="bahasa" id="language">English <br>
            <input type="checkbox" name="bahasa" id="language">Other <br> <br>

            <label for="biodata">Bio</label> <br>
            <textarea name="bio" id="biodata" cols="50" rows="7" required></textarea> <br>

            <input type="submit" value="Sign Up">
        </form>
    @endsection