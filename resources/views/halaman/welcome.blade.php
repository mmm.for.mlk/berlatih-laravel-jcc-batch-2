@extends('layout.master')

@section('judul')
    Berhasil Mendaftar
@endsection
    
@section('content')
    <h4>SELAMAT DATANG, <br><br> {{"$nama $belakang"}} </h4>
    <br>
    <h6>Terima kasih telah bergabung di Sanberbook. Social Media Belajar kita bersama!</h6>
@endsection