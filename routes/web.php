<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');

Route::get('/register', 'AuthController@register');

Route::post('/send', 'AuthController@welcome');

Route::get('/data-tables', function(){
    return view('halaman.data-table');
});

Route::get('/table', function(){
    return view('halaman.table');
});


// CRUD
// Mengarah ke form create data
Route::get('/cast/create', 'CastController@create');
// Menyimpan data ke table cast
Route::post('/cast', 'CastController@store');
// Menampilkan semua data
Route::get('/cast', 'CastController@index');
//Menampilkan detail data
Route::get('/cast/{cast_id}', 'CastController@show');
//Menampilkan form edit
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//Update data ke table
Route::put('/cast/{cast_id}', 'CastController@update');
//Delete data
Route::delete('/cast/{cast_id}', 'CastController@destroy');